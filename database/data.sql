-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 06, 2023 at 05:23 PM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data`
--
CREATE DATABASE IF NOT EXISTS `data` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `data`;

-- --------------------------------------------------------

--
-- Table structure for table `image_library`
--

DROP TABLE IF EXISTS `image_library`;
CREATE TABLE IF NOT EXISTS `image_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `image_library`
--

INSERT INTO `image_library` (`id`, `product_id`, `path`, `created_time`, `last_updated`) VALUES
(18, 21, 'uploads/06-01-2023/daychuyentraitim03.jpg', 1672994314, 1672994314),
(19, 21, 'uploads/06-01-2023/daychuyentraitim02.jpg', 1672994340, 1672994340),
(20, 21, 'uploads/06-01-2023/daychuyentraitim.jpg', 1672994372, 1672994372),
(21, 22, 'uploads/06-01-2023/bongtaingoctrai2.jpg', 1672994523, 1672994523),
(22, 22, 'uploads/06-01-2023/bongtaingoctrai3.jpg', 1672994536, 1672994536),
(23, 23, 'uploads/06-01-2023/bongtaititan2.jpg', 1672994700, 1672994700),
(24, 23, 'uploads/06-01-2023/bongtaitian3.jpg', 1672994714, 1672994714),
(25, 24, 'uploads/06-01-2023/bongtaithongoc2.jpg', 1672994929, 1672994929),
(26, 25, 'uploads/06-01-2023/lactaythanhgia01.jpg', 1672995026, 1672995026),
(27, 25, 'uploads/06-01-2023/lactaythanhgia02.jpg', 1672995041, 1672995041),
(28, 25, 'uploads/06-01-2023/lactaythanhgia03.jpg', 1672995057, 1672995057),
(29, 26, 'uploads/06-01-2023/lactaytitan1.jpg', 1672995232, 1672995232),
(30, 26, 'uploads/06-01-2023/lactaytitan2.jpg', 1672995247, 1672995247),
(31, 26, 'uploads/06-01-2023/lactaytitan3.jpg', 1672995266, 1672995266),
(32, 27, 'uploads/06-01-2023/nhan3lop1.jpg', 1673024563, 1673024563),
(33, 27, 'uploads/06-01-2023/nha3lop2.jpg', 1673024588, 1673024588),
(34, 27, 'uploads/06-01-2023/nhan3lop3.jpg', 1673024605, 1673024605),
(35, 28, 'uploads/06-01-2023/nhan4lop2(1).jpg', 1673024879, 1673024879),
(36, 28, 'uploads/06-01-2023/nhan4lop3(1).jpg', 1673024895, 1673024895),
(37, 29, 'uploads/06-01-2023/vonghat3.jpg', 1673025097, 1673025097),
(38, 29, 'uploads/06-01-2023/vonghat4.jpg', 1673025110, 1673025110),
(39, 29, 'uploads/06-01-2023/vonghat5.jpg', 1673025121, 1673025121),
(40, 30, 'uploads/06-01-2023/bongtaikhongtuoc2.jpg', 1673025330, 1673025330),
(41, 31, 'uploads/06-01-2023/vonglove2.jpg', 1673025552, 1673025552),
(42, 31, 'uploads/06-01-2023/vonglove3.jpg', 1673025566, 1673025566),
(43, 32, 'uploads/06-01-2023/nhannguyetque2.jpg', 1673025706, 1673025706),
(44, 32, 'uploads/06-01-2023/nhannguyetque3.jpg', 1673025720, 1673025720);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) NOT NULL,
  `link` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `parent_id`, `name`, `link`, `position`, `created_time`, `last_updated`) VALUES
(4, 0, 'Cấp 1', 'home2.php', 0, 1656333459, 1656333459),
(5, 4, 'Cấp 2', 'product.php', 1, 1656333459, 1656333459),
(6, 5, 'Cấp 3', 'product.php', 0, 1656333459, 1656333459),
(7, 6, 'Cấp 4', 'home.php', 0, 1656333459, 1656333459),
(8, 4, 'Cấp 2.2', 'product.php', 2, 1656333459, 1656333459),
(9, 8, 'Cấp 3.2', 'product.php', 1, 1656333459, 1656333459),
(10, 7, 'Cấp 5', 'home.php', 0, 1656333459, 1656333459),
(20, 17, 'Cấp 7', '#', 1, 1656333459, 1656333459),
(21, 16, 'Cấp 2.2.2', 'home2.php', 1, 1656333459, 1656333459),
(17, 10, 'Cấp 6', '#', 1, 1656333459, 1656333459),
(16, 0, 'Cấp 1.2', 'home2.php', 1, 1656333459, 1656333459);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `addres` varchar(500) NOT NULL,
  `note` text NOT NULL,
  `total` int(11) NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `name`, `phone`, `addres`, `note`, `total`, `created_time`, `last_updated`) VALUES
(29, 'Admin', '0777755342', 'HCM', 'Ghi chu', 8990000, 1656333459, 1656333459);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE IF NOT EXISTS `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `quantity`, `price`, `created_time`, `last_updated`) VALUES
(19, 29, 2, 1, 540000, 1587872426, 1587872426),
(20, 29, 18, 3, 1450000, 1587872426, 1587872426),
(21, 29, 20, 4, 850000, 1587872426, 1587872426);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `price` float NOT NULL,
  `content` text NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `image`, `price`, `content`, `created_time`, `last_updated`) VALUES
(21, 'Kiềng Cổ Dây Chuyền Hình Trái Tim Titan Ko Han Rỉ TT 4361', 'uploads/06-01-2023/daychuyentraitim01.jpg', 140000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4361\r\n-Màu sắc: vàng\r\n-Kiểu dáng: dạng kiềng\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: free size', 1672994314, 1672994372),
(22, 'Bông Tai Tua Rua Dài Ngọc Trai Và Đá Zircon Titan Ko Han Rỉ TT 4343', 'uploads/06-01-2023/bongtaingoctrai.jpg', 120000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4343\r\n-Màu sắc: vàng hồng\r\n-Kiểu dáng: khuyên dài\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: free size', 1672994523, 1672994536),
(23, 'Bông Tai Titan Ko Đen TT 4206', 'uploads/06-01-2023/bongtaititan.jpg', 120000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4206\r\n-Màu sắc: vàng\r\n-Kiểu dáng: mảnh\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: free size', 1672994700, 1672994714),
(24, 'Bông Tai Thỏ Ngọc Chất Liệu Titan Không Han Rỉ TT4497', 'uploads/06-01-2023/bongtaithongoc.jpg', 95000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT4497\r\n-Màu sắc: vàng, vàng hồng, bạc\r\n-Kiểu dáng: khuyên nụ\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: free size', 1672994929, 1672994929),
(25, 'Lắc Tay Dây Bi Mảnh Thánh Giá Titan Ko Han Rỉ TT 4360', 'uploads/06-01-2023/lactaythanhgia.jpg', 120000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4360\r\n-Màu sắc: bạc, vàng\r\n-Kiểu dáng: dây bi mảnh\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: 17+6cm', 1672995026, 1672995057),
(26, 'Lắc Tay Dây Xích Nữ Titan Ko Han Rỉ TT 4359', 'uploads/06-01-2023/lactaytitan.jpg', 95000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4359\r\n-Màu sắc: bạc\r\n-Kiểu dáng: dây xích đơn giản\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: 19cm (free size) bản 5.5mm', 1672995232, 1672995266),
(27, 'Nhẫn Nữ Cá Tính 3 Lớp Titan Ko Đen TT 4336', 'uploads/06-01-2023/nhan3lop.jpg', 110000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4336\r\n-Màu sắc: vàng\r\n-Kiểu dáng: bản to\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: size 6 đường kính 16mm, size 7-17mm size 8-18mm', 1673024563, 1673024718),
(28, 'Nhẫn Nữ Cá Tính 4 Lớp Titan Ko Đen TT 4334', 'uploads/06-01-2023/nhan4lop(1).jpg', 120000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4334\r\n-Màu sắc: vàng\r\n-Kiểu dáng: bản to, nhẫn hở\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: free size', 1673024879, 1673024895),
(29, 'Lắc Tay Nữ Hạt Rỗng Trang Sức Xuping Mạ Vàng Siêu Bền XP4517', 'uploads/06-01-2023/vonghat.jpg', 130000, '-Chất liệu: đồng mạ vàng, với công nghệ mạ 3 lớp siêu bền màu và ít dị ứng\r\n-Mã sản phẩm: XP4517\r\n-Màu sắc: vàng 24K, trắng\r\n-Kiểu dáng: cổ điển\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: 17+4cm\r\n', 1673025097, 1673025121),
(30, 'Bông Tai Khoen Tròn Đá Khổng Tước Chất Liệu Titan Không Han Rỉ TT 4471', 'uploads/06-01-2023/bongtaikhongtuoc3.jpg', 165000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4471\r\n-Màu sắc: vàng\r\n-Kiểu dáng: khoen tròn\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: khoen vàng 2.4cm, khoen xanh 1.4cm', 1673025330, 1673025339),
(31, 'Lắc Tay LOVE Dây Hạt Gạo Chất Liệu Titan Không Han Rỉ TT 4467', 'uploads/06-01-2023/vonglove.jpg', 170000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4467\r\n-Màu sắc: vàng\r\n-Kiểu dáng: dây rút\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: 15+5cm', 1673025552, 1673025566),
(32, 'Nhẫn Nữ Vòng Nguyệt Quế Chất Liệu Titan Không Han Rỉ TT 4464', 'uploads/06-01-2023/nhannguyetque.jpg', 200000, '-Chất liệu: titan không đen không dị ứng, hoàn toàn an toàn cho sức khỏe\r\n-Mã sản phẩm: TT 4464\r\n-Màu sắc: vàng, bạc\r\n-Kiểu dáng: nhẫn hở\r\n-Sử dụng: thích hợp đi chơi, đi làm, dự tiệc\r\n-Kích thước: free size', 1673025706, 1673025720);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `addres` varchar(100) NOT NULL,
  `created_time` int(11) NOT NULL,
  `last_updated` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `phone`, `addres`, `created_time`, `last_updated`) VALUES
(1, 'admin', '25d55ad283aa400af464c76d713c07ad', 'admin@admin.vn', '0232313323', '123 Hữu Nghĩa, P1, Q1, HCM', 123, 1656333459);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `image_library`
--
ALTER TABLE `image_library`
  ADD CONSTRAINT `image_library_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
