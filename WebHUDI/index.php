<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet"  type="text/css" href="style.css">
    <link rel="stylesheet" type="text/css" href="khung.css"/>
    <link rel="stylesheet" type="text/css" href="menu.css"/>
    <title> Loading... </title>
</head>
<body>
        <?php
                include './connect_db.php';
                //UP sản phẩm hot
                $item_per_page = !empty($_GET['per_page'])?$_GET['per_page']:20;
                $current_page = !empty($_GET['page'])?$_GET['page']:1; //Trang hiện tại
                $offset = ($current_page - 1) * $item_per_page;
                $products = mysqli_query($con, "SELECT * FROM `product` ORDER BY `id` ASC  LIMIT " . $item_per_page . " OFFSET " . $offset);
                $totalRecords = mysqli_query($con, "SELECT * FROM `product`");
                $totalRecords = $totalRecords->num_rows;
                $totalPages = ceil($totalRecords / $item_per_page);
                
                ?>
<div class="tongkhung">
<div class="big">
            
	 <div id="header">
           
                <img src="uploads/hinh/logo-HUDI.svg" width="200px">
                <form action="search.php" method="get">
                Tìm kiếm <input type="text" name="search" />
                <input class="btn-search" type="submit" name="ok" value="Tìm sản phẩm"  />
            </form>
            <div id="menu">
                <div class="item">
                    <a href="index.php">TRANG CHỦ</a>
                </div>
                <div class="item">
                    <a href="gioithieu.html">GIỚI THIỆU</a>
                </div>
                <div class="item">
                    <a href="cart.php">GIỎ HÀNG</a>
                </div>
                <div class="item">
                    <a href="admin/index.php">ĐĂNG NHẬP</a>
             </div>
          <div id="actions">
            <div class="item">
              <img src="uploads/hinh/heart-solid-24.png" alt="">
             
              <img src="uploads/hinh/search-alt-2-regular-24.png"alt="">
              <img src="uploads/hinh/bx-cart-alt.svg" alt="" /></div>
            </div>
            </div>
        </div>
 </div>   	
                    
                   
                            
                                <?php
                                while ($row = mysqli_fetch_array($products)) {
                                    ?>
                                    <div class="product-item">
                                        <div class="product-img">
                                        <a class="idsanpham"  href="detail.php?id=<?= $row['id'] ?>"><img src="<?= $row['image'] ?>" title="<?= $row['name'] ?>" /></a>
                                        </div>
                                        <strong><a  href="detail.php?id=<?= $row['id'] ?>"><?= $row['name'] ?></a></strong><br/>
                               <div class="dongia">Giá:<span class="product-price"><?= number_format($row['price'], 0, ",", ".") ?> Đ</span><br/></div>
                                        <p><?= $row['content'] ?></p>
                                        <div class="nut">
                                           
                                            
                                            <div class="buy-button">
                                                <a class="code_a" href="detail.php?id=<?= $row['id'] ?>" style="--clr:#ff9800"><span> Chi tiết </span><i></i></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php 
								} 
								?>
                            
                                    
               
                
  </div>  

 <div class="footer">
 <h4>Copyright © All rights reserved.</h4>
 </div>   
</body>
</html>