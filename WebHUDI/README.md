Họ tên sinh viên : Đặng Lương Tuấn Cường Lớp : 22C2B-LTM1 Mô tả project
: Trang web bán trang sức cho cửa hàng HUDi ( Với các chức năng Đăng
nhập , Thêm , Xóa , Sửa , Tìm kiếm )

Đồ án cá nhân sử dụng thuần PHP

Yêu cầu : 
Cần có Wampserver hoặc Xampp 
Cần có công cụ IDE : DreamWeaver , Vscode , PHPStorm,....

Hướng dẫn sử dụng : 
    1.Đầu tiên cần mở Wampserver hoặc Xampp, mở MySQL tạo một database với tên "data" ,nhấp vào mục Import 
    Nhấn vào phần"Choose file" mở folder database trong source code,chọn file "data" 
    Sau đó nhấn vào nút "Go"

    2.Mở một IDE bất kỳ chọn path và đường dẫn folder vào source code 

    3.Cuối cùng là mở file index.php và nhấn Open in Browser chọn một trình duyệt bất kỳ

    Tài khoản để đăng nhập trang Quản Lý :
    Username :admin
    Password :12345678
